//
//  UIViewExtension.swift
//  AddressBook
//
//  Created by Julien on 04/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get { return layer.shadowRadius }
        set {
            layer.shadowRadius = newValue
            if newValue != 0 {
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOpacity = 0.3
                layer.shadowOffset = .zero
            }
        }
    }
}
