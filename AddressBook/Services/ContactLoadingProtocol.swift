//
//  ContactLoadingProtocol.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 03/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation

protocol ContactLoadingProtocol {
  func loadContacts(_ completion: @escaping (Result<([Contact], Bool), ContactLoadingError>) -> Void)
}

enum ContactLoadingError: Error {
  case noInternet
  case apiError
  case noData
  case parsingError

  var localizedDescription: String {
    switch self {
    case .noInternet: return "No Internet Connection"
    case .apiError: return "An error occured (API)"
    case .noData: return "An error occured (no data)"
    case .parsingError: return "An error occured (invalid data)"
    }
  }
}
