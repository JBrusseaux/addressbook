//
//  OfflineStoreProtocol.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 06/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation

protocol OfflineStoreProtocol {
  var storedData: [Contact] { get }
  func loadStoredData(_ completion: @escaping ([Contact]) -> Void)
  func updateStoredData(_ data: [Contact])
}
