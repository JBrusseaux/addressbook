//
//  ContactLoader.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 03/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation

final class ContactLoader: ContactLoadingProtocol {
  static let defaultLoader = ContactLoader(endpoint: "https://randomuser.me/api/", itemsPerCall: 10)

  let endpoint: String
  let itemsPerCall: Int
  
  init(endpoint: String, itemsPerCall: Int) {
    self.endpoint = endpoint
    self.itemsPerCall = itemsPerCall
  }
  
  func loadContacts(_ completion: @escaping (Result<([Contact], Bool), ContactLoadingError>) -> Void) {
    if let url = URL(string: "\(endpoint)?results=\(itemsPerCall)") {
      let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
        guard let self = self else { return }
        
        if error != nil {
          completion(.failure(.apiError))
        } else if let data = data {
          if let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any],
            let results = json["results"] as? [Any] {
            
            let contactDataList = results.compactMap { try? JSONSerialization.data(withJSONObject: $0) }
            let contacts = contactDataList.compactMap { try? JSONDecoder().decode(Contact.self, from: $0) }
            
            completion(.success((contacts, results.count < self.itemsPerCall)))
          } else {
            completion(.failure(.parsingError))
          }
        } else {
          completion(.failure(.noData))
        }
      }
      task.resume()
    }
  }
}
