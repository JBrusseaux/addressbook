//
//  OfflineStore.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 06/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

class OfflineStore: OfflineStoreProtocol {

  static let defaultStore = OfflineStore()

  private(set) var storedData = [Contact]()

  private let storePath = "Contacts.json"

  func loadStoredData(_ completion: @escaping ([Contact]) -> Void) {
    let fileManager = FileManager.default
    if let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
      let storeUrl = documentsUrl.appendingPathComponent(storePath)
      if fileManager.fileExists(atPath: storeUrl.path),
        let data = try? Data(contentsOf: storeUrl),
        let contacts = try? JSONDecoder().decode([Contact].self, from: data) {
        self.storedData = contacts
        completion(contacts)
        return
      }
    }
    completion([])
  }

  func updateStoredData(_ contacts: [Contact]) {
    storedData = contacts
    let fileManager = FileManager.default
    if let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
      let storeUrl = documentsUrl.appendingPathComponent(storePath)
      if let data = try? JSONEncoder().encode(contacts) {
        try? data.write(to: storeUrl)
        self.storedData = contacts
      }
    }
  }
}
