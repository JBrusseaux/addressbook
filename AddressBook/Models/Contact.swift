//
//  Contact.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 03/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation

final class Contact: Codable {
  let title: String
  let firstName: String
  let lastName: String
  let address: Address
  let email: String
  let phone: String
  let cellphone: String
  let nationality: String
  let thumbnail: String
  let mediumPicture: String
  let largePicture: String


  var fullName: String {
    return "\(firstName) \(lastName)"
  }

  enum CodingKeys: String, CodingKey {
    case address = "location"
    case email
    case phone
    case cellphone = "cell"
    case nationality = "nat"
    case name
    case picture
  }

  enum NameKeys: String, CodingKey {
    case firstName = "first"
    case lastName = "last"
    case title
  }

  enum PictureKeys: String, CodingKey {
    case thumbnail
    case mediumPicture = "medium"
    case largePicture = "large"
  }


  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    address = try values.decode(Address.self, forKey: .address)
    email = try values.decode(String.self, forKey: .email)
    phone = try values.decode(String.self, forKey: .phone)
    cellphone = try values.decode(String.self, forKey: .cellphone)
    nationality = try values.decode(String.self, forKey: .nationality)

    let nameValues = try values.nestedContainer(keyedBy: NameKeys.self, forKey: .name)
    title = try nameValues.decode(String.self, forKey: .title)
    firstName = try nameValues.decode(String.self, forKey: .firstName)
    lastName = try nameValues.decode(String.self, forKey: .lastName)

    let pictureValues = try values.nestedContainer(keyedBy: PictureKeys.self, forKey: .picture)
    thumbnail = try pictureValues.decode(String.self, forKey: .thumbnail)
    mediumPicture = try pictureValues.decode(String.self, forKey: .mediumPicture)
    largePicture = try pictureValues.decode(String.self, forKey: .largePicture)
  }

  func encode(to encoder: Encoder) throws {
    var values = encoder.container(keyedBy: CodingKeys.self)
    try values.encode(address, forKey: .address)
    try values.encode(email, forKey: .email)
    try values.encode(phone, forKey: .phone)
    try values.encode(cellphone, forKey: .cellphone)
    try values.encode(nationality, forKey: .nationality)

    var nameValues = values.superEncoder(forKey: .name).container(keyedBy: NameKeys.self)
    try nameValues.encode(title, forKey: .title)
    try nameValues.encode(firstName, forKey: .firstName)
    try nameValues.encode(lastName, forKey: .lastName)

    var pictureValues = values.superEncoder(forKey: .picture).container(keyedBy: PictureKeys.self)
    try pictureValues.encode(thumbnail, forKey: .thumbnail)
    try pictureValues.encode(mediumPicture, forKey: .mediumPicture)
    try pictureValues.encode(largePicture, forKey: .largePicture)
  }
}
