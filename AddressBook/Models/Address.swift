//
//  Address.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 05/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import CoreLocation
import Foundation

final class Address: Codable {
  var streetNumber: Int
  var streetName: String
  var city: String
  var country: String
  var postcode: Int
  var latitude: Double
  var longitude: Double

  private var street: String?
  private var coordinates: String?

  func formattedString() -> String {
    return "\(streetNumber) \(streetName)\n\(postcode) \(city)\n\(country)"
  }

  func geocodingTerms() -> String {
    return "\(streetName) \(city) \(country)"
  }
  
  enum CodingKeys: String, CodingKey {
    case city
    case country
    case postcode
    case street
    case coordinates
  }

  enum StreetKeys: String, CodingKey {
    case number
    case name
  }

  enum CoordinatesKeys: String, CodingKey {
    case latitude
    case longitude
  }

  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    city = try values.decode(String.self, forKey: .city)
    country = try values.decode(String.self, forKey: .country)
    postcode = try values.decode(Int.self, forKey: .postcode)

    let streetValues = try values.nestedContainer(keyedBy: StreetKeys.self, forKey: .street)
    streetNumber = try streetValues.decode(Int.self, forKey: .number)
    streetName = try streetValues.decode(String.self, forKey: .name)

    let coordinateValues = try values.nestedContainer(keyedBy: CoordinatesKeys.self, forKey: .coordinates)
    latitude = Double(try coordinateValues.decode(String.self, forKey: .latitude)) ?? 0
    longitude = Double(try coordinateValues.decode(String.self, forKey: .longitude)) ?? 0
  }

  func encode(to encoder: Encoder) throws {
    var values = encoder.container(keyedBy: CodingKeys.self)
    try values.encode(city, forKey: .city)
    try values.encode(country, forKey: .country)
    try values.encode(postcode, forKey: .postcode)

    var streetValues = values.superEncoder(forKey: .street).container(keyedBy: StreetKeys.self)
    try streetValues.encode(streetNumber, forKey: .number)
    try streetValues.encode(streetName, forKey: .name)

    var coordinateValues = values.superEncoder(forKey: .coordinates).container(keyedBy: CoordinatesKeys.self)
    try coordinateValues.encode(String(latitude), forKey: .latitude)
    try coordinateValues.encode(String(longitude), forKey: .longitude)
  }
}
