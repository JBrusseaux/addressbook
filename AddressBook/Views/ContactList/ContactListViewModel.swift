//
//  ContactListViewModel.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 03/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation
import RxCocoa

final class ContactListViewModel {
  let contacts = BehaviorRelay<[ContactViewModel]>(value: [])
  let loadingFinished = BehaviorRelay<Bool>(value: false)
}
