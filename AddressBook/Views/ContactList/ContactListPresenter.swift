//
//  ContactListPresenter.swift
//  AddressBook
//
//  Created by Julien on 04/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

protocol ContactListPresenterView: UIViewController {
  func showError(_ error: ContactLoadingError)
}

final class ContactListPresenter {
  weak var view: ContactListPresenterView?
  let viewModel = ContactListViewModel()

  private let offlineStore: OfflineStoreProtocol
  private let loader: ContactLoadingProtocol

  init(_ view: ContactListPresenterView, offlineStore: OfflineStoreProtocol, loader: ContactLoadingProtocol) {
    self.offlineStore = offlineStore
    self.loader = loader
    self.view = view
  }

  func loadView() {
    offlineStore.loadStoredData { [weak self] contacts in
      self?.viewModel.contacts.accept(contacts.map { ContactViewModel($0) })
      self?.viewModel.loadingFinished.accept(true)
    }
  }

  func loadNextContacts() {
    loader.loadContacts { [weak self] result in
      guard let self = self else { return }

      switch result {
      case let .success(contacts, finishedLoading):
        self.offlineStore.updateStoredData(self.offlineStore.storedData + contacts)
        self.viewModel.contacts.accept(self.offlineStore.storedData.map { ContactViewModel($0) })
        self.viewModel.loadingFinished.accept(finishedLoading)
      case let .failure(error):
        self.view?.showError(error)
      }
    }
  }

  @objc func reload() {
    offlineStore.updateStoredData([])
    loadNextContacts()
  }

  func showDetail(_ viewModel: ContactViewModel) {
    DispatchQueue.main.async {
      self.view?.present(ContactDetailViewController(viewModel), animated: true)
    }
  }
}
