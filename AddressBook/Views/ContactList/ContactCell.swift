//
//  ContactCell.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 03/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import RxKingfisher
import RxSwift
import UIKit

final class ContactCell: UITableViewCell {
  
  @IBOutlet private weak var contactContentView: UIView!
  @IBOutlet private weak var contactImageView: UIImageView!
  @IBOutlet private weak var contactNameLabel: UILabel!
  @IBOutlet private weak var flagImageView: UIImageView!

  let disposeBag = DisposeBag()

  func setup(with viewModel: ContactViewModel) {
    viewModel.name.bind(to: contactNameLabel.rx.text).disposed(by: disposeBag)
    viewModel.picture
        .bind(to: contactImageView.kf.rx.image(options: [.transition(.fade(0.2))]))
        .disposed(by: disposeBag)
    viewModel.flag.bind(to: flagImageView.rx.image).disposed(by: disposeBag)
  }
    
}
