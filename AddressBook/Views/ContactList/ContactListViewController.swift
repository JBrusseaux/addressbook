//
//  ContactListViewController.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 03/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

final class ContactListViewController: UIViewController, ContactListPresenterView {

  @IBOutlet private weak var tableView: UITableView!

  private let scrolledToBottom = PublishRelay<Void>()

  let disposeBag = DisposeBag()
  lazy var presenter = ContactListPresenter(self, offlineStore: OfflineStore.defaultStore, loader: ContactLoader.defaultLoader)
  
  override func viewDidLoad() {
    super.viewDidLoad()

    tableView.register(UINib(nibName: "ContactCell", bundle: nil), forCellReuseIdentifier: "ContactCell")
    setupRefresh()
    setupFooter()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    bindViewModel()
    presenter.loadView()
  }

  func setupRefresh() {
    tableView.refreshControl = UIRefreshControl()
    tableView.refreshControl?.addTarget(presenter, action: #selector(ContactListPresenter.reload), for: .valueChanged)
  }

  private func setupFooter() {
    let footer = UIView(frame: CGRect(origin: .zero, size: CGSize(width: view.bounds.width, height: 60)))
    let activity = UIActivityIndicatorView(style: .medium)
    footer.addSubview(activity)
    activity.center = footer.center
    activity.startAnimating()
    activity.color = .gray
    tableView.tableFooterView = footer

    scrolledToBottom
      .throttle(.seconds(2), scheduler: MainScheduler.instance)
      .subscribe(onNext: { _ in
        print("scrolledToBottom")
        self.presenter.loadNextContacts()
      })
      .disposed(by: disposeBag)
  }

  private func bindViewModel() {
    presenter.viewModel.contacts
      .bind(to: tableView.rx.items(cellIdentifier: "ContactCell")) { row, model, cell in
        (cell as? ContactCell)?.setup(with: model)
    }
    .disposed(by: disposeBag)
    presenter.viewModel.contacts
      .subscribe(onNext: { [weak self] _ in
        DispatchQueue.main.async {
          self?.tableView.refreshControl?.endRefreshing()
        }
      })
    .disposed(by: disposeBag)
    tableView.rx.itemSelected
      .subscribe(onNext: { indexPath in
        let item = self.presenter.viewModel.contacts.value[indexPath.row]
        self.presenter.showDetail(item)
      })
      .disposed(by: disposeBag)
  }

  func showError(_ error: ContactLoadingError) {
    DispatchQueue.main.async {
      let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default))
      self.present(alert, animated: true)
    }
  }
}

extension ContactListViewController: UITableViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if let footerFrame = tableView.tableFooterView?.frame,
      view.frame.contains(footerFrame.offsetBy(dx: 0, dy: -tableView.contentOffset.y)) {
      scrolledToBottom.accept(())
    }
  }
}
