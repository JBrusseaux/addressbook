//
//  ContactViewModel.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 03/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation
import CoreLocation
import Kingfisher
import RxCocoa

final class ContactViewModel {
    let name: BehaviorRelay<String>
    let address: BehaviorRelay<String>
    let email: BehaviorRelay<String>
    let phone: BehaviorRelay<String>
    let cellPhone: BehaviorRelay<String>
    let picture: BehaviorRelay<Resource?>
    let thumbnail: BehaviorRelay<Resource?>
    let flag: BehaviorRelay<UIImage?>
    let geocoding: BehaviorRelay<String>
    let coordinate: BehaviorRelay<CLLocationCoordinate2D>
    
  init(_ contact: Contact) {
    name = BehaviorRelay<String>(value: contact.fullName)
    address = BehaviorRelay<String>(value: contact.address.formattedString())
    email = BehaviorRelay<String>(value: contact.email)
    phone = BehaviorRelay<String>(value: contact.phone)
    cellPhone = BehaviorRelay<String>(value: contact.cellphone)
    picture = BehaviorRelay<Resource?>(value: URL(string: contact.mediumPicture))
    thumbnail = BehaviorRelay<Resource?>(value: URL(string: contact.thumbnail))
    flag = BehaviorRelay<UIImage?>(value: UIImage(named: contact.nationality.lowercased()))
    geocoding = BehaviorRelay<String>(value: contact.address.geocodingTerms())
    coordinate = BehaviorRelay<CLLocationCoordinate2D>(value: CLLocationCoordinate2D(latitude: contact.address.latitude,
                                                                                      longitude: contact.address.longitude))
  }
}
