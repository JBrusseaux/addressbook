//
//  ContactDetailViewController.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 04/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import MapKit
import RxCocoa
import RxSwift
import UIKit

final class ContactDetailViewController: UIViewController, ContactDetailPresenterView {
  @IBOutlet private weak var container: UIView!
  @IBOutlet private weak var frontView: UIView!
  @IBOutlet private weak var backView: UIView!
  @IBOutlet private weak var backButton: UIButton!
  @IBOutlet private weak var mapView: MKMapView!
  @IBOutlet private weak var contactPicture: UIImageView!
  @IBOutlet private weak var contactName: UILabel!
  @IBOutlet private weak var contactMail: UIButton!
  @IBOutlet private weak var contactAddress: UIButton!
  @IBOutlet private weak var contactCellPhone: UIButton!
  @IBOutlet private weak var contactPhone: UIButton!

  @IBOutlet weak var hiddenConstraint: NSLayoutConstraint?
  @IBOutlet weak var visibleConstraint: NSLayoutConstraint?

  var presenter: ContactDetailPresenter?

  let disposeBag = DisposeBag()

  init(_ viewModel: ContactViewModel) {
    super.init(nibName: nil, bundle: nil)

    presenter = ContactDetailPresenter(self, viewModel: viewModel)
    self.modalPresentationStyle = .overFullScreen
    self.transitioningDelegate = self
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override public func viewDidLoad() {
    super.viewDidLoad()

    let feedback = UIImpactFeedbackGenerator(style: .medium)
    feedback.impactOccurred()

    view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
    let tapGR = UITapGestureRecognizer()
    tapGR.delegate = self
    view.addGestureRecognizer(tapGR)
    tapGR.rx.event
      .bind { _ in
        self.dismiss(animated: true)
    }
    .disposed(by: disposeBag)

    contactAddress.titleLabel?.numberOfLines = 3
    contactAddress.titleLabel?.textAlignment = .center
    backButton.rx.tap
      .subscribe(onNext: { _ in
        self.flip()
      })
      .disposed(by: disposeBag)
    bindViewModel()
    addMotionEffect()
  }

  private func bindViewModel() {
    presenter?.viewModel.picture
      .bind(to: contactPicture.kf.rx.image(options: [.transition(.fade(0.2))]))
      .disposed(by: disposeBag)
    presenter?.viewModel.name.bind(to: contactName.rx.text).disposed(by: disposeBag)
    presenter?.viewModel.email.bind(to: contactMail.rx.title()).disposed(by: disposeBag)
    presenter?.viewModel.address.bind(to: contactAddress.rx.title()).disposed(by: disposeBag)
    presenter?.viewModel.phone.bind(to: contactPhone.rx.title()).disposed(by: disposeBag)
    presenter?.viewModel.cellPhone.bind(to: contactCellPhone.rx.title()).disposed(by: disposeBag)

    contactMail.rx.tap
      .subscribe(onNext: { _ in
        self.presenter?.openMail()
      })
      .disposed(by: disposeBag)
    contactAddress.rx.tap
      .subscribe(onNext: { _ in
        self.presenter?.openAddress()
      })
      .disposed(by: disposeBag)
    contactCellPhone.rx.tap
      .subscribe(onNext: { _ in self
        .presenter?.openCellPhone()
      })
      .disposed(by: disposeBag)
    contactPhone.rx.tap
      .subscribe(onNext: { _ in
        self.presenter?.openPhone()
      })
      .disposed(by: disposeBag)
  }

  func addMotionEffect() {
    let effectX = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
    let effectY = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
    let group = UIMotionEffectGroup()
    group.motionEffects = [effectX, effectY]
    group.motionEffects?.compactMap({ $0 as? UIInterpolatingMotionEffect })
      .forEach({
        $0.minimumRelativeValue = -50
        $0.maximumRelativeValue = 50
      })
    container.addMotionEffect(group)
  }

  func flip(_ completion: (() -> Void)? = nil) {
    UIView.transition(with: container, duration: 0.5, options: [.transitionFlipFromLeft], animations: {
      self.frontView.isHidden = !self.frontView.isHidden
      self.backView.isHidden = !self.backView.isHidden
    }) { _ in
      completion?()
    }
  }

  func show(address: String, coordinate: CLLocationCoordinate2D) {
    CLGeocoder().geocodeAddressString(address) { (placemarks, error) in
      if let coordinate2 = placemarks?.first?.location?.coordinate {
        self.mapView.setRegion(MKCoordinateRegion(center: coordinate2, latitudinalMeters: 1000, longitudinalMeters: 1000), animated: false)
        self.flip {
          let annotation = MKPointAnnotation()
          annotation.coordinate = coordinate2
          self.mapView.addAnnotation(annotation)
        }

        // Fallback on coordinate provided by the API
      } else {
        self.mapView.setRegion(MKCoordinateRegion(center: coordinate, latitudinalMeters: 100000, longitudinalMeters: 100000), animated: false)
        self.flip {
          let annotation = MKPointAnnotation()
          annotation.coordinate = coordinate
          self.mapView.addAnnotation(annotation)
        }
      }
    }
  }

  func showError(_ error: Error) {
    DispatchQueue.main.async {
      let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default))
      self.present(alert, animated: true)
    }
  }
}

/******************************************************/
// MARK: - UIGestureRecognizerDelegate
extension ContactDetailViewController: UIGestureRecognizerDelegate {
  public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
    return touch.view == self.view
  }
}
