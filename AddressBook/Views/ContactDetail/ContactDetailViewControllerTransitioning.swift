//
//  ContactDetailViewControllerTransitioning.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 05/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

/******************************************************/
// MARK: - UIViewControllerTransitioningDelegate
extension ContactDetailViewController: UIViewControllerTransitioningDelegate {
  public func animationController(forPresented presented: UIViewController,
                                  presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return ModalShowController()
  }

  public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return ModalDismissController()
  }
}

/******************************************************/
// MARK: - Animation
private class ModalShowController: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.4
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let modal = transitionContext.viewController(forKey: .to) as? ContactDetailViewController else { return }
    transitionContext.containerView.addSubview(modal.view)
    modal.view.frame = transitionContext.containerView.bounds
    modal.view.alpha = 0
    modal.visibleConstraint?.isActive = false
    modal.hiddenConstraint?.isActive = true
    modal.view.layoutSubviews()
    UIView.animate(withDuration: 0.1, animations: {
      modal.view.alpha = 1
    }, completion: { _ in
      modal.hiddenConstraint?.isActive = false
      modal.visibleConstraint?.isActive = true
      UIView.animate(withDuration: 0.3, animations: {
        modal.view.layoutIfNeeded()
      }, completion: { _ in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
      })
    })
  }
}

private class ModalDismissController: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.5
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let modal = transitionContext.viewController(forKey: .from) as? ContactDetailViewController else { return }
    modal.visibleConstraint?.isActive = false
    modal.hiddenConstraint?.isActive = true
    UIView.animate(withDuration: 0.3, animations: {
      modal.view.layoutIfNeeded()
    }, completion: { _ in
      UIView.animate(withDuration: 0.1, animations: {
        modal.view.alpha = 0
      }, completion: { _ in
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
      })
    })
  }
}
