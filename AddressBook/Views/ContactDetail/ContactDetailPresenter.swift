//
//  ContactDetailPresenter.swift
//  AddressBook
//
//  Created by Julien Brusseaux on 04/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import CoreLocation
import MessageUI
import UIKit

protocol ContactDetailPresenterView: UIViewController {
  func show(address: String, coordinate: CLLocationCoordinate2D)
  func showError(_ error: Error)
}

final class ContactDetailPresenter: NSObject {
  weak var view: ContactDetailPresenterView?
  let viewModel: ContactViewModel

  init(_ view: ContactDetailPresenterView, viewModel: ContactViewModel) {
    self.view = view
    self.viewModel = viewModel
  }

  func openMail() {
    let mailController = MFMailComposeViewController()
    mailController.mailComposeDelegate = self
    mailController.setToRecipients([viewModel.email.value])
    view?.present(mailController, animated: true)
  }

  func openAddress() {
    view?.show(address: viewModel.geocoding.value, coordinate: viewModel.coordinate.value)
  }

  func openCellPhone() {
    if let url = URL(string: "tel://\(viewModel.cellPhone.value)"),
      UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }
  }

  func openPhone() {
    if let url = URL(string: "tel://\(viewModel.phone.value)"),
      UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url)
    }
  }
}

extension ContactDetailPresenter: MFMailComposeViewControllerDelegate {
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true)
  }
}
